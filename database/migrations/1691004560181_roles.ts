import BaseSchema from '@ioc:Adonis/Lucid/Schema'
import Roles from 'App/Enums/Roles'

export default class extends BaseSchema {
  protected tableName = 'roles'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.string('role_name', 10).notNullable()
      table.timestamp('created_at', { useTz: true }).notNullable().defaultTo(this.now())

      this.defer(async (db) => {
        await db.table(this.tableName).multiInsert([
          {
            id: Roles.USER,
            role_name: "USER"
          },
          {
            id: Roles.ADMIN,
            role_name: "ADMIN"
          }
        ])
      })
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
