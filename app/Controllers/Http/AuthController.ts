import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import LoginValidator from 'App/Validators/LoginValidator'

export default class AuthController {
  public async login({request, response, auth}: HttpContextContractd) {
    try {
      const data = await request.validate(LoginValidator)
      
      const token = await auth.use('api').attemp(data.email, data.password)

      return response.ok({
        message: 'login berhasil!',
        token: token.token
      })
    } catch (error) {
      if(error.responseText) {
        return response.unauthorized({message: 'Email atau Password salah!'})
      } else if(error.messages) {
        return response.badRequest({message: error.messages.errors})
      } else {
        console.log(error)
      }
    }
  }
}
